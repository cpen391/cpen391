var app = require('express')();
var http = require('http').Server(app)
var io = require('socket.io')(http)

// Port 3000 is configurable to any type of connection - this is important!
var server = http.listen(3000, () => {
	var port = server.address().port;
	console.log(`Example app listening at ${port}`)
})

//Whenever someone connects this gets executed.
io.on('connection', (socket) => {
	console.log('A user connected: ' + socket.id);
	var channel = socket.handshake.query.channel;
	console.log('A user connected to channel: ' + channel);

	socket.join(channel)
	console.log(`User ${socket.id} has joined channel: ${channel}`)

	socket.on('emit_to_channel', message => {
		console.log(`message from client received: ${message}`);
		let data = {"message":message, "socket_id":socket.id};
		io.to(channel).emit('channel_message',data);
	});

	//Whenever someone disconnects this piece of code gets executed.
	socket.on('disconnect', () => {
		console.log(`A user disconnected: ${socket.id}, leaving channel: ${channel}`);
		socket.leave(channel)
	});

});

app.get('/', (req, res) => {
	res.sendFile(__dirname + '/index.html')
})
