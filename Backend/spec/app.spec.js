/*
 * Simple Backend test to ensure the backend performs as it should. 
 * The backend should simply:
 * * receive socket requests
 * * add sockets to rooms
 * * forward messages to channels
 * * remove sockets from channels on disconnect
 * Note that the below tests require the server to be running. 
*/
const io = require('socket.io-client');
const endpoint = "http://3.137.200.137:3000";
const channel_name_1 = "bryson_channel";
const message = "Hey Channel!";

describe("ClientSocketIO", function() {
	it("should not be null", function() {
		let socket = io(endpoint, {
			query: { "channel": channel_name_1}
		})
		expect(socket).not.toBeNull();
	}); 
	it("should connect successfully", function() {
		let socket = io(endpoint, {
			query: { "channel": channel_name_1}
		})
		socket.on("connect", () => { 
			expect(socket.connected).toEqual(true);
		});
	}); 
	it("should receive message that it sends", function() {
		let socket = io(endpoint, {
			query: { "channel": channel_name_1}
		})
		socket.on("connect", () => { 
			socket.emit("emit_to_channel", message);
			socket.on('channel_message', data => {
				expect(data.message).toEqual(message);
			});
		});
	}); 
});

