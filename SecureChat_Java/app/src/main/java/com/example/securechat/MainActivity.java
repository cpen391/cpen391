package com.example.securechat;

import androidx.appcompat.app.AppCompatActivity;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;

import android.content.Intent;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.util.Set;

public class MainActivity extends AppCompatActivity {
    BluetoothService btService;// = BluetoothService.getInstance();
    private ImageButton btnBluetooth; // The button user clicks to connect bluetooth devices.
    private Button btnChannel; // The button user clicks to submit channel name.
    private EditText nickname;
    public static final String NICKNAME = "usernickname";
    private Context context;
    private final int RED = Color.parseColor("#FF4D4D");
    private final int GREEN = Color.parseColor("#90EE90");
    private final int PAIRED_COLOR = Color.parseColor("#FFFFB3");
    private final String HWConnectedText = "Device is connected";
    private final String HWNotConnectedText = "Device is not connected";
    private final String HWConnectedPairedText = "Device is paired, but not connected";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        context = getApplicationContext();

        btService = BluetoothService.getInstance();
        if(!btService.bluetoothSupported()){
            Toast.makeText(context, "Bluetooth not supported on this device!", Toast.LENGTH_LONG);
            finish();
            System.out.println("Killed the app.");
        }

        // Set the frontend to indicate the state of rfs connection.
//        btService.disconnectFromRFS();
        setHardwareIndicatorView();

        //call UI components  by id
        btnBluetooth = (ImageButton)findViewById(R.id.connect_bluetooth) ;
        btnChannel = (Button)findViewById(R.id.enterchat) ; //TODO
        nickname = (EditText) findViewById(R.id.nickname);

        nickname.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if ((event != null && (event.getKeyCode() == KeyEvent.KEYCODE_ENTER)) || (actionId == EditorInfo.IME_ACTION_DONE)) {
                    //do what you want on the press of 'done'
                    btnChannel.performClick();
                }
                return false;
            }
        });

        /**
         * Callback for when a user submits a channelName to enter
         */
        btnChannel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //TODO deal with edge case where the RFS board is paired but its not serialConnected (this is the case when the device has been paired in the past but is not turned on and in range).
                if(!btService.isRfsConnected()){
                    Toast.makeText(context, "Connect the RFS board before continuing, click on the bluetooth button below.", Toast.LENGTH_LONG).show();
                    setHardwareIndicatorView();
                } else if(!nickname.getText().toString().isEmpty()) {
                    Intent i = new Intent(MainActivity.this, ChatBoxActivity.class);
                    //retreive nickname from EditText and add it to intent extra
                    i.putExtra(NICKNAME, nickname.getText().toString());
                    startActivity(i);
                }else{
                    Toast.makeText(context, "Enter a title!", Toast.LENGTH_LONG).show();
                    setHardwareIndicatorView();
                }
            }
        });


        /**
         * Callback for when a user clicks bluetooth set up button.
         */
        btnBluetooth.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MainActivity.this, BluetoothActivity.class);
                //retrieve nickname from EditText and add it to intent extra
                startActivity(i);
            }
        });
    }

    public void setViewHardwareConnected(){
        TextView hwConnectText = (TextView) findViewById(R.id.hwConnection);
        hwConnectText.setText(HWConnectedText);
        hwConnectText.setBackgroundColor(GREEN);
    }

    public void setViewHardwarePaired(){
        TextView hwConnectText = (TextView) findViewById(R.id.hwConnection);
        hwConnectText.setText(HWConnectedPairedText);
        hwConnectText.setBackgroundColor(PAIRED_COLOR);
    }

    public void setViewHardwareNotConnected(){
        TextView hwConnectText = (TextView) findViewById(R.id.hwConnection);
        hwConnectText.setText(HWNotConnectedText);
        hwConnectText.setBackgroundColor(RED);
    }

    public void setHardwareIndicatorView() {
        if (btService.isRfsConnected())
            setViewHardwareConnected();
        else if (btService.isRfsPaired())
            setViewHardwarePaired();
        else
            setViewHardwareNotConnected();
    }

    public void deleteCache(Context context) {
        try {
            File dir = context.getCacheDir();
            deleteDir(dir);
        } catch (Exception e) { e.printStackTrace();}
    }

    public boolean deleteDir(File dir) {
        if (dir != null && dir.isDirectory()) {
            String[] children = dir.list();
            for (int i = 0; i < children.length; i++) {
                boolean success = deleteDir(new File(dir, children[i]));
                if (!success) {
                    return false;
                }
            }
            return dir.delete();
        } else if(dir!= null && dir.isFile()) {
            return dir.delete();
        } else {
            return false;
        }
    }

}