package com.example.securechat;
import java.math.BigInteger;
import java.util.Scanner;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import 	androidx.recyclerview.widget.DefaultItemAnimator;
import 	androidx.recyclerview.widget.LinearLayoutManager;
import 	androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

import io.socket.client.IO;
import io.socket.client.Socket;
import io.socket.emitter.Emitter;
import org.json.JSONException;
import org.json.JSONObject;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;

public class ChatBoxActivity extends AppCompatActivity {
    public RecyclerView myRecylerView ;
    public List<Message> MessageList ;
    public ChatBoxAdapter chatBoxAdapter;
    public  EditText messagetxt ;
    public  Button send ;
    public String url;
    private BluetoothService btService = BluetoothService.getInstance();
    public String ChannelMsg;

    //declare socket object
    private Socket socket;
    private String Nickname ;
    private TextView ChannelID;

    public void printToHex(String arg) {
        System.out.println("YOLO PRINTING");
        for(char c: arg.toCharArray()){
           String hex = String.format("%04x", (int)c);
            System.out.println(hex);
        }
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO - change this endpoint to current AWS server IP
        URI uri = URI.create("http://3.137.200.137:3000");

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat_box);

        messagetxt = (EditText) findViewById(R.id.message);
        send = (Button)findViewById(R.id.send);

        // get the nickame of the user
        Nickname=(String)getIntent().getExtras().getString(MainActivity.NICKNAME);

        try {
            IO.Options mOptions = IO.Options.builder()
                    .setQuery("channel=" + Nickname)
                    .build();
            mOptions.forceNew=true;
            socket = IO.socket(uri, mOptions);
            socket.connect();
        } catch (Exception e) {
            Log.d("STATE",e.toString());
            e.printStackTrace();
        }

        ChannelID = (TextView)findViewById(R.id.ChannelText);

        ChannelMsg = "Joined Channel : " + Nickname;

        ChannelID.setText(ChannelMsg);

        // Setting up a Recycler view for the dynamic messages lists
        MessageList = new ArrayList<>();
        myRecylerView = (RecyclerView) findViewById(R.id.messagelist);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        myRecylerView.setLayoutManager(mLayoutManager);
        myRecylerView.setItemAnimator(new DefaultItemAnimator());

        /**
         * Callback for when user sends a message
         * TODO - check if bluetooth is connected or not
         * TODO - if connected: dispatch to encryption via bluetooth
         * TODO - if not connected: prompt user to connect to RFS via bluetooth.
         */
        send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //retrieve the nickname and the message content and fire the event

//                ProgressBar pgsBar = (ProgressBar)findViewById(R.id.pBar);
//                pgsBar.setVisibility(v.VISIBLE);

                if(messagetxt.getText().toString().isEmpty()) return;
                String message = messagetxt.getText().toString();
                System.out.println("MESSAGE to encrypt= "+ message);
                String encryptedMessage = btService.encrypt(message);
                System.out.println("ENCRYPTED MESSAGE= "+ encryptedMessage);
                socket.emit("emit_to_channel", encryptedMessage);
                messagetxt.setText(" "); // Reset input box on view
            }
        });


        /**
         * Let the user know when a new user has joined the channel.
         * TODO - implement the event "userjoinedthechat" on backend
         */
//        socket.on("userjoinedthechat", new Emitter.Listener() {
//            @Override
//            public void call(final Object... args) {
//                runOnUiThread(new Runnable() {
//                    @Override
//                    public void run() {
//                        String data = (String) args[0];
//                        Toast.makeText(ChatBoxActivity.this,data,Toast.LENGTH_SHORT).show();
//                    }
//                });
//            }
//        });

        /**
         * Let the user know when a user has left the channel
         * TODO - implement the event "userdisconnect" on backend
         */
//        socket.on("userdisconnect", new Emitter.Listener() {
//            @Override
//            public void call(final Object... args) {
//                runOnUiThread(new Runnable() {
//                    @Override
//                    public void run() {
//                        String data = (String) args[0];
//                        Toast.makeText(ChatBoxActivity.this,data,Toast.LENGTH_SHORT).show();
//                    }
//                });
//            }
//        });


        /**
         * Receive a message from the backend server via socket. Update view to show message.
         * TODO - check if bluetooth is connected or not
         * TODO - if connected: dispatch to decryption via bluetooth
         * TODO - if not connected: prompt user to connect to RFS via bluetooth.
         */
        socket.on("channel_message", new Emitter.Listener() {
            @Override
            public void call(final Object... args) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        JSONObject data = (JSONObject) args[0];
                        try {
                            //extract data from fired event
                            String nickname = data.getString("socket_id");
                            String message = data.getString("message");

                            printToHex(message);
                            //Decrypt the message in the de1soc
                            System.out.println("Message to decrypt: "+ message);
                            String decryptedMessage = btService.decrypt(message);
                            System.out.println("Message decrypted: "+ decryptedMessage);


                            // make instance of message
                            Message m = new Message(nickname,decryptedMessage);

                            // add the message to the messageList
                            MessageList.add(m);

                            // add the new updated list to the adapter
                            chatBoxAdapter = new ChatBoxAdapter(MessageList);

                            // notify the adapter to update the recycler view
                            chatBoxAdapter.notifyDataSetChanged();

                            //set the adapter for the recycler view
                            myRecylerView.setAdapter(chatBoxAdapter);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        socket.disconnect();
    }
}
