package com.example.securechat;
import android.Manifest;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.Toast;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import java.util.ArrayList;

public class BluetoothActivity extends AppCompatActivity {

    private final static int REQUEST_ENABLE_BT = 1;

    BluetoothService btService = BluetoothService.getInstance();
    private ImageButton refreshBtn;
    private Context context;
    /** Variables for the discovered devices! **/
    private BroadcastReceiver mReceiver;// handle to BroadCastReceiver object
    private ArrayList<BluetoothDevice> Discovereddevices = new ArrayList <BluetoothDevice> ( ) ;
    private ArrayList<String> myDiscoveredDevicesStringArray = new ArrayList <String> ( ) ;
    private ArrayAdapter<String> discoveredDevicesAdapter;
    private AdapterView.OnItemClickListener mDiscoveredClickedHandler = new AdapterView.OnItemClickListener()
    {
        @Override
        public void onItemClick(AdapterView<?> adapter, View v, int position, long arg3)
        {
            BluetoothDevice device = Discovereddevices.get(position);
            String toastText = "Trying to connect to: " + device.getName() + "...";
            Toast.makeText(context, toastText, Toast.LENGTH_LONG).show();
            boolean connected = btService.connectRFS(device);
            if(connected){
                // Route user back to home page.
                Intent i = new Intent(BluetoothActivity.this, MainActivity.class);
                startActivity(i);
            }else
                Toast.makeText(context, "Failed to connect to " + device.getName(), Toast.LENGTH_LONG).show(); // create popup for device
            discoveredDevicesAdapter.notifyDataSetChanged();
        }
    };

    private int androidVersion;
    private final int REQUEST_ID = 3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setContentView(R.layout.activity_bluetooth);
        context = getApplicationContext();
        refreshBtn = (ImageButton)findViewById(R.id.refreshButton);
        // Request Permissions for using bluetooth!
        this.requestPermissions();

        ListView discoveredlistView = (ListView) findViewById(R.id.discoveredDevicesList);
        discoveredlistView.setOnItemClickListener ( mDiscoveredClickedHandler );
        discoveredDevicesAdapter = new ArrayAdapter<>(context, android.R.layout.simple_list_item_1, myDiscoveredDevicesStringArray);
        discoveredlistView.setAdapter(discoveredDevicesAdapter);

        if (!btService.bluetoothEnabled()) {
            Intent enableBtIntent = new Intent ( BluetoothAdapter.ACTION_REQUEST_ENABLE );
            // REQUEST_ENABLE_BT below is a constant (defined as '1 - but could be anything)
            // When the “activity” is run and finishes, Android will run your onActivityResult()
            // function (see next page) where you can determine if it was successful or not
            startActivityForResult (enableBtIntent, REQUEST_ENABLE_BT);
        }

        mReceiver = new BroadcastReceiver() {
            public void onReceive(Context context, Intent intent) {
                String action = intent.getAction();
                BluetoothDevice newDevice;
                if ( action.equals(BluetoothDevice.ACTION_FOUND) ) {
                    newDevice = intent.getParcelableExtra ( BluetoothDevice.EXTRA_DEVICE );
                    String theDevice = new String( newDevice.getName()+"\nMAC Address = " + newDevice.getAddress());
                    Discovereddevices.add ( newDevice );
                    myDiscoveredDevicesStringArray.add ( theDevice );
                    discoveredDevicesAdapter.notifyDataSetChanged();
                }else if (action.equals(BluetoothAdapter.ACTION_DISCOVERY_STARTED)) {
                    Toast.makeText(context, "Discovery Started", Toast.LENGTH_LONG).show();
                }else if (action.equals(BluetoothAdapter.ACTION_DISCOVERY_FINISHED)) {
                    Toast.makeText(context, "Discovery Finished", Toast.LENGTH_LONG).show();
                }
            }
        };

        // create 3 separate IntentFilters that are tuned to listen to certain Android broadcasts
        // 1) when new bluetooth_colour devices are discovered,
        // 2) when discovery of devices starts (not essential but give useful feedback)
        // 3) When discovery ends (not essential but give useful feedback)
        IntentFilter filterFound = new IntentFilter (BluetoothDevice.ACTION_FOUND);
        IntentFilter filterStart = new IntentFilter (BluetoothAdapter.ACTION_DISCOVERY_STARTED);
        IntentFilter filterStop = new IntentFilter(BluetoothAdapter.ACTION_DISCOVERY_FINISHED);
        // register our broadcast receiver so it gets called every time
        // a new bluetooth device is found or discovery starts or finishes
        // we should unregister it again when the app ends in onDestroy() - see later
        context.registerReceiver (mReceiver, filterFound);
        context.registerReceiver (mReceiver, filterStart);
        context.registerReceiver (mReceiver, filterStop);

        // now start scanning for new devices. The broadcast receiver
        btService.startDiscovering();

        refreshBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Discovereddevices.clear();
                myDiscoveredDevicesStringArray.clear();
                discoveredDevicesAdapter.notifyDataSetChanged();
                btService.startDiscovering();
            }
        });
        super.onCreate(savedInstanceState);
    }


    /**
     * For mobile devices above a certain version number we need to request permission
     * to use bluetooth and be able to discover other devices close by.
     */
    private void requestPermissions(){
        androidVersion = Build.VERSION.SDK_INT;
        if (androidVersion >= 23){
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION,
                            Manifest.permission.ACCESS_COARSE_LOCATION,
                    }, REQUEST_ID);
        }
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_ENABLE_BT)
            if (resultCode != RESULT_OK) {
                Toast toast = Toast.makeText(context, "BlueTooth Failed to Start ", Toast.LENGTH_LONG);
                toast.show();
                finish();
                return;
            }
        super.onActivityResult(requestCode, resultCode, data);
    }



    /**
     * When user naviagtes away from bluetooth page, this is called to unregister the bluetooth notification receiver.
     */
    public void onDestroy() {
        super.onDestroy();
        try {
            // Try and catch so that the app doesn't crash when user goes back. - bryson
            unregisterReceiver ( mReceiver ); // make sure we unregister
        } catch(IllegalArgumentException e) {
            System.out.println("Error unregistering the reciever.");
            e.printStackTrace();
        }
    }

}