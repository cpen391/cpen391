package com.example.securechat;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.os.SystemClock;
import android.util.Log;
import android.widget.Toast;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.Method;
import java.util.Set;
import java.util.UUID;

import io.socket.client.IO;

public class BluetoothService {
   private static final BluetoothService ourInstance = new BluetoothService();
   private BluetoothAdapter mBluetoothAdapter;
   private String rfsName = "HC-05";
   public BluetoothDevice rfsDevice; //The rfs board as a Bluetooth Device.
   private boolean rfsSerialConnected = false; // RFS is connected via a serial connection to the phone (more than just paired).
   // a “socket” to a blue tooth device
   private BluetoothSocket mmSocket = null;
   // input/output “streams” with which we can read and write to device
   public InputStream mmInStream;
   public OutputStream mmOutStream;

   private BluetoothService(){
      try{
         mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
         if(searchForRFSInPairedDevices()) {
            System.out.println("Constructor of service: " + rfsDevice.getName() + "Trying to connect to RFS");
            rfsSerialConnected = connectRFS(rfsDevice);

         }else
            System.out.println("Constructor of service: NO RFS FOUND");
      }catch(NullPointerException e){
          System.out.println("Bluetooth not supported!");
          e.printStackTrace();
      }
   }

   /**
    * Method to obtain the single static instance.
    * @return the singleton instance.
    */
   public static BluetoothService getInstance(){
      return ourInstance;
   }

   /**
    * Attempt to connect a bluetooth device to the app. Assume the device is the RFS Board.
    * @param rfsBoard the BluetoothDevice of the rfs board.
    * @return True if successfully connects, false otherwise.
    */
   public boolean connectRFS(BluetoothDevice rfsBoard){
      try{
//        closeConnection();
        CreateSerialBluetoothDeviceSocket(rfsBoard);
        ConnectToSerialBlueToothDevice();
        rfsSerialConnected = true;
      }catch (IOException e){
        System.out.println("Failed to connect to the bluetooth device with name: " + rfsBoard.getName());
        e.printStackTrace();
        rfsSerialConnected = false;
      }
      return rfsSerialConnected;
   }

   /**
    * Encrypt a message by sending it to the hardware (DE1SOC) and blocking for a response.
    * @param message the plaintext message to encrypt.
    * @return The encrypted message as a String.
    */
   public String encrypt(String message){
      if(!rfsSerialConnected) connectRFS(rfsDevice);
      String rcvText = "";
      try {
         WriteToBTDevice("^e" + message); //setup
         //Wait for encrypted message back from RFS
         System.out.println("Waiting response from rfs encrypt...");
         //TODO this is where Owen and I will need to change to configure the communication between DE1 and phone.
         while (rcvText.equals(""))
            rcvText = ReadFromBTDevice();
         System.out.println("Received response encrypt: " + rcvText);
      }catch (IOException e){
         System.out.println("Failed to encrypt message!");
         e.printStackTrace();
         return null;
      }
      return rcvText;
   }

   /**
    * Decrypt an encrypted message by sending it to the hardware (DE1SOC) and blocking for a response.
    * @param message the encrypted string message to decrypt.
    * @return The plaintext message as a string.
    */
   public String decrypt(String message){
      if(!rfsSerialConnected) connectRFS(rfsDevice);
      String rcvText = "";
      try {
         WriteToBTDevice("^d"+message); //setup
         //Wait for decrypted message back from RFS
         System.out.println("Waiting response from rfs...");
         //TODO this is where Owen and I will need to change to configure the communication between DE1 and phone.
         while(rcvText.equals(""))
            rcvText = ReadFromBTDevice();
         System.out.println("Received response:" + rcvText + "NOTTHEMESSAGE");
      }catch (IOException e){
         System.out.println("Failed to decrypt message!");
         e.printStackTrace();
         return null;
      }
      return rcvText;
   }

   /**
    * Wrapper method for the BluetoothAdaptor discorvering method.
    */
   public void startDiscovering(){
      if(mBluetoothAdapter.isDiscovering()) stopDiscovering();
      mBluetoothAdapter.startDiscovery();
   }

   /**
    * Wrapper method for the BluetoothAdaptor cancelDiscovery method.
    */
   public void stopDiscovering(){
      mBluetoothAdapter.cancelDiscovery();
   }

   /**
    * Learn if the device supports bluetooth.
    * @return boolean, true if supported, false if not.
    */
   public boolean bluetoothSupported(){
      return mBluetoothAdapter != null;
   }

   /**
    * Learn if the device has bluetooth enabled.
    * @return boolean, true if enabled, false if not.
    */
   public boolean bluetoothEnabled(){
     return mBluetoothAdapter != null && mBluetoothAdapter.isEnabled();
   }

   /**
    * Check is the RFS board is paired and serially connected (ready to send and receive messages via bluetooth serial connection).
    * @return boolean if connected.
    */
   public boolean isRfsConnected(){
      return isRfsPaired() && rfsSerialConnected;
   }

   /**
    * Check is the RFS board is paired (could still not be serially connected and ready to send/receive data).
    * @return boolean if paired.
    */
   public boolean isRfsPaired(){
      return ((rfsDevice != null) || searchForRFSInPairedDevices());
   }

   /**
    * Helper method to find the RFS board in the list of this androids paired devices.
    * Uses the name of the Bluetooth device to match the RFS board.
    * @return true if found, false otherwise.
    */
  private boolean searchForRFSInPairedDevices(){
     Set<BluetoothDevice> devices = mBluetoothAdapter.getBondedDevices();
     for(BluetoothDevice d : devices){
        if(d.getName().contains(rfsName)){
           System.out.println("Name: " + d.getName() + " ADDRESS: " + d.getAddress() + " BondState: " + d.getBondState() + " Type: " + d.getType());
           rfsDevice = d;
        }
     }
     if(rfsDevice != null)
        System.out.println("Constructor of service: " + rfsDevice.getName());
     else
        System.out.println("Constructor of service: NO RFS FOUND");
     return rfsDevice != null;
  }

   /**
    * Close a connection to a bluetooth device.
    * Closes and nulls all the IoStreams.
    */
   private void closeConnection() {
      try {
         if(mmInStream != null)
            mmInStream.close();
         mmInStream = null;
      } catch (IOException e) {}
      try {
         if(mmOutStream != null)
            mmOutStream.close();
         mmOutStream = null;
      } catch (IOException e) {}
      try {
         if(mmSocket != null)
            mmSocket.close();
         mmSocket = null;
      } catch (IOException e) {}
      rfsSerialConnected = false;
   }

   /**
    * Creates a serial bluetooth socket connection.
    * @param device - the device to connect to.
    */
   private void CreateSerialBluetoothDeviceSocket(BluetoothDevice device) throws IOException
   {
      mmSocket = null;
      // universal UUID for a serial profile RFCOMM blue tooth device
      // this is just one of those “things” that you have to do and just works
      UUID MY_UUID = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");
      // Get a bluetooth_colour Socket to connect with the given BluetoothDevice
      // MY_UUID is the app's UUID string, also used by the server code
      mmSocket = device.createRfcommSocketToServiceRecord(MY_UUID);
   }

   /**
    * Creates a serial connection via bluetooth to a bluetooth device.
    */
   private void ConnectToSerialBlueToothDevice()  throws IOException{
      // Cancel discovery because it will slow down the connection
      mBluetoothAdapter.cancelDiscovery();

      // Attempt connection to the device through the socket.
      mmSocket.connect();
      System.out.println("Bluetooth Connection Made");

      //create the input/output stream and record fact we have made a connection
      GetInputOutputStreamsForSocket();
   }

   /**
    * Write the to the Bluetooth device.
    * @param message the message to send to the bluetooth device.
    */
   public void WriteToBTDevice (String message) throws IOException{
      String newlineString = "\r\n";
      byte[] newlineBytes = newlineString.getBytes();
      byte[] msgBuffer = stringToBytes(message);
      System.out.println("MESSAGE BUFFER LENGTH : " + msgBuffer.length);
      String numberOfBytes = String.format("^%03d", msgBuffer.length); //Pad the array with zeros.
      System.out.println("NUMBER OF BYTES: " + numberOfBytes);
      byte[] numOfBytes = numberOfBytes.getBytes();
      mmOutStream.write(numOfBytes);
      mmOutStream.write(newlineBytes);
      mmOutStream.write(msgBuffer);
   }

   /**
    * Convert the string message into an array of bytes perserving the data values.
    * @param msg the string to convert
    * @return Array of bytes.
    */
   public byte[] stringToBytes(String msg){
      byte[] bytes = new byte[msg.length()];
      char[] chars = msg.toCharArray();
      for(int i = 0; i < chars.length; i++){
         bytes[i] = (byte)chars[i];
      }
      return bytes;
   }

   /**
    * Read from the Bluetooth device.
    * @return  String- the message sent from Bluetooth device.
    */
   public String ReadFromBTDevice() throws IOException{
      byte c;
      String s = "";
      System.out.println("READING THE BYETS");
      for (int i = 0; i < 200; i++) { // try to read for 2 seconds max
         SystemClock.sleep(10);
         if (mmInStream.available() > 0) {
            if ((c = (byte) mmInStream.read()) != '\r') { // '\r' terminator
               System.out.println(c);
               s += (char) c; // build up string 1 byte by byte
            }else
               return s;
         }
      }
      return s;
   }

   /**
    * Gets the input/output stream associated with the current socket.
    */
   public void GetInputOutputStreamsForSocket() throws IOException {
      System.out.println("SETTING INPUT/OUTPUT STREAMS");
      mmInStream = mmSocket.getInputStream();
      mmOutStream = mmSocket.getOutputStream();
   }

   public void disconnectFromRFS() {
      try {
         if(!isRfsPaired()) return;
         BluetoothDevice device = rfsDevice;
         Method m = device.getClass().getMethod("removeBond", (Class[]) null);
         m.invoke(device, (Object[]) null);
         System.out.println("SUCCESS TO UNPAIR");
      } catch (Exception e) {
         System.out.println("FAILED TO UNPAIR");
         e.printStackTrace();
      }
   }
}
