# SecureChat CPEN 391 Repo
This is a repo that contains two different subsystems for our project called SecureChat.
This repo was created by a team of UBC undergraduate students for a class called CPEN391. 

## Directory SecureChat\_Java
* The java android app which is the main interface for our project. 
* Interfaces with node backend and DE1SOC via bluetooth.

## Directory Backend
* The Node Backend code which creates our open source backend using socketIO.
* Runs on an AWS ubuntu server.

